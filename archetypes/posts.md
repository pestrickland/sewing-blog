---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
tags:
    - Technique
    - Research
draft: true
---

Intro text...

## Section 1

Blah blah blah...

## Section 2

On the other hand, blah blah blah...

## Summary

Summary...
