---
title: "{{ replace (substr .Name 4 ) "-" " " | title }}"
date: {{ .Date }}
number: "{{ substr .Name 0 3 }}"
made: {{ .Date }}
fabric:
pattern:
colour:
tags:
    - garment type
    - commercial pattern
draft: true
---

Intro text...

## Design

Design details...

## Construction

Construction details...

## Summary

Summary...
