---
title: "Sky Blue Jackson Tee"
date: 2021-11-23T22:08:13Z
number: "005"
made: 2021-10-09
fabric: "[Premium Cotton Jersey from Minerva](https://www.minerva.com/mp/1193825/premium-cotton-jersey-stretch-knit-fabric-pastel-blue&variant=1131409)"
pattern: "[Jackson](/resources/patterns/jackson-tee)"
colour: Sky/pastel blue
tags:
  - t-shirt
  - indie pattern
---

My third t-shirt, and so far my favourite.

## Design

The Jackson Tee from Helen's Closet is available with long or short sleeves and as a t-shirt or a jumper. I chose a t-shirt with short sleeves and aimed for a slightly over-sized fit. I didn't do anything fancy with the pattern -- just cut a bigger size than I would do usually.

## Construction

I approached the construction of this with some confidence, having made two t-shirts previously. I decided to make it directly on my overlocker rather than sew the seams together first. I also used some bulky thread in the loopers to see if there was a noticeable difference. Finally, I didn't have a close enough match on my ribbing to be happy to use it on the neckband, so instead I cut that from the main/self fabric.

### Neckband in Self-fabric

I thought I might end up with a tight-fitting neckband, or else one that was hard to pull on or off. In the event, neither happened and I'm perfectly happy with the result. Whether due to practice, the use of the overlocker only or the choice of fabric, I also got my best result so far when easing the the neckband onto the shirt.

### Twin Needle

I picked up a twin-needle to sew the hems on this t-shirt and it worked quite well. I don't think I got the tension quite right, but I am pleased with the result and only found one skipped stitch

### Winding Bulky Thread

I previously picked up a couple of cones of bulky thread and thought that the best way to use them on my overlocker was to wind some bobbins so that my upper and lower loopers would be the same colour. I made the mistake of winding it too tightly, however, and had numerous problems with the thread breaking and getting stuck on the bobbin. Eventually I managed to wind a bobbin with as little tension as possible but without having to wind it by hand (I did use my hand to "tension" the thread though, rather than the machine's tension disks).

## Summary

I was very pleased with putting this together, and with the result. As planned, it fits me in a slightly over-sized way, with the shoulder seams just falling off my shoulders. And once again, I am very pleased with this fabric -- it has a really premium quality feel to it, and I'll be making more with it in future.

It's quite interesting looking back at my previous makes to see my progress. This t-shirt is by far the best of the three, and even though I thought I was compromising without using ribbing for the neckband, it looks so much more like a bought t-shirt, and a nice one at that. So pleased was I with it, in fact, that I showed it off at a pub meal with friends last week, and promptly stained it with bacon jam!
