---
title: "Merchant & Mills Anthracite T-shirt"
date: 2021-04-10T22:57:22+01:00
number: "002"
made: 2021-04-10T22:57:22+01:00
fabric: "[Premium Cotton Jersey from Minerva](https://www.minerva.com/mp/1193825/premium-cotton-jersey-knit-fabric-anthracite-grey&variant=1131395)"
pattern: https://merchantandmills.com/store/patterns/pdf/the-tee-shirt-2/
colour: Anthracite with dark grey ribbing
tags:
  - t-shirt
  - indie pattern
---

My first garment made with a knit fabric was this t-shirt using a pattern from Merchant & Mills. It took me longer to make than it should have done, but I re-stitched some pieces that I wasn't happy with. I'm glad I did – I'm really pleased with the final result.

{{<figure src="002-finished-t-shirt.jpg" title="The finished item" >}}

## Design

The Merchant Mills _Tee Shirt_ includes a front pocket and sleeves that can be rolled up (thanks to a large cuff on the inside). In terms of fit, I found it to be pretty good for me in the 38" size. It's possibly a little large compared my normal sizing; my suit size is 38-40" and I'd choose between small and medium with most casual brands of t-shirt. If anything, I could have cut the t-shirt narrower but longer, but that's being quite picky.

## Construction

This was my first attempt at sewing cotton jersey and at times that got a bit frustrating. Overall, however, it was lovely to work with and I got more confident as I progressed.

The pocket was constructed first, and I struggled with this on my mum's computerised machine (but I think I incorrectly threaded it). I was really annoyed to get some some skipped stitches. I also didn't trim enough of the bulk out of the pocket so it sits quite proud.

{{<figure src="002-pocket.jpg" title="There are a few skipped stitches and the pocket is a bit bulky at the seams" >}}

Once the pocket was sewn onto the front the shoulder seams were sewn and finished. This was also done using my mum's sewing machine before I'd fixed the threading confusion, so again there were some skipped stitches on the seam.

{{<figure src="002-shoulder-seam.jpg" title="The shoulder seam has some skipped stitches too but the overlocking and edge stitching is reasonable" >}}

I did consider constructing the seams on the overlocker but opted to sew first before finishing the seams. This gave me more flexibility to adjust the construction if needed. Once finished, the seams were all pressed back and stitched down. This edge stitching was quite tricky to do without a reliable guide line, but I was pleased with the results.

{{<figure src="002-side-seam.jpg" title="I'm very pleased with my efforts on the inside of the t-shirt!" >}}

Fixing the neckband was the most awkward and probably the worst part of my attempt. After stabbing many times with pins, the first time I sewed on the neckband I caught the front in an annoying pucker.

{{<figure src="002-pucker.jpg" title="Annoying little pucker" >}}

In general I found it quite difficult to distribute the front and back evenly around the neckband. But the pucker really annoyed me, so I took the neckband off and tried again. It still wasn't perfect but it was a much better attempt.

The final problem I had with the neckband came when I finished the seam. Despite taking care with the overlocker I managed to go too far around and ended up cutting off the start of the overlock. In a bit of a bodge, I added a short section of extra overlock to hold everything in place.

{{<figure src="002-neckband.jpg" title="I needed to add a bit of extra overlock once I noticed that I'd cutting through what I'd just sewn" >}}

Adding the sleeves was relatively easy and they even seem to be about the same length. The hems, however, were fiddly, and the needle missed some of the raw edge on one of the sleeves and the bottom of the shirt. I unpicked those and did them again more successfully.

## Summary

If I was surprised at being able to successfully construct a pair of pyjama bottoms, completing this t-shirt was even more impressive to me, mainly because it looks just like any other t-shirt I own. I really felt that I understood more about what I was doing too, and the sequence of operations was more familiar too. And the result that comes from sewing a seam, finishing it, pressing it back and then sewing it down made me feel quite pleased with myself.

The t-shirt certainly isn't perfect – there are some wobbly lines of top-stitching, and a bit too much bulk in the pocket and where the hems are sewn over a finished seam. Regardless, I'm very pleased.

### Next steps

I'm going to make this pattern again. I want to see if I can make something repeatably, since I wear a lot of similar clothes. I would also like to get more comfortable with the process. Following that, I might look to altering the pattern. Some options are:

- Lengthen the shirt
- Move the pocket inwards slightly
- Take the shirt in at the sides a bit
