---
title: "Hudson Pants"
date: 2021-06-30T19:57:51Z
number: "003"
made: 2021-06-27T15:00:39Z
fabric: "Petrol fleece from local shop"
pattern: "[True Bias Men's Hudson Pant](https://shop.truebias.com/product/men-s-hudson-pant)"
colour: Petrol
tags:
  - trousers
  - indie pattern
draft: false
---

The Hudson Pants pattern is one that I have been keen to try for a while, although making it just as summer begins to warm up might not have been the best idea... However, a quality pair of sweatpants (we used to call them jogging bottoms when I grew up but the idea of running in them seems absurd) has become a staple for my work wardrobe in the past year. My go-to pair so far has been the [French Terry Sweatpant](https://www.everlane.com/products/mens-french-terry-sweatpant-uniform-navy?collection=mens-sweatshirts) from [Everlane](https://www.everlane.com/); these are at the smarter end of the scale, but I still doubt I'd wear them out and about.

## Design

There are a couple of interesting design features that caught my attention:

- Tapered legs
- Pocket trim
- No ribbing on the ankle cuffs

Rather than measuring in detail, I went for my usual size of 32 without any adjustment. The instructions said that the pattern was drafted for a 6-ft model so I thought that would be fine. In hindsight I would consider a larger size or at least longer legs, as they come up a bit short. My fabric wasn't particularly stretchy either which led to quite a tight fit when putting them on.

## Construction

Except for getting confused with the pocket trim right at the start, I found these very straightforward to construct. The ⅜" seam allowance and the use of a knit fabric means that you could quite easily get away without finishing the seams, but I overlocked the lot for practice.

Compared to my last trouser construction (Simplicity 8179 pyjamas), one of the great parts of these instructions was the construction of the waistband. It is joined together before attaching to the trousers, and this means that the elastic can be added through that seam before closing it (and overlocking if you're finishing the seams). This made a lot more sense to me than having to close the waistband by hand -- much faster too.

That said, I cut and sewed my elastic too long -- there was hardly any gather and despite the fit being quite slim, these did not want to stay up when I tried them on. Shortening the elastic easily solved that, and the low rise of this pattern became much less of a concern. I still think that they are slightly too small for me but with the waist staying put they felt much better.
