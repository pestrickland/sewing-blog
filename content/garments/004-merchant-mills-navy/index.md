---
title: "Merchant & Mills Navy T-shirt"
date: 2021-12-01T22:05:29Z
number: "004"
made: 2021-05-01
fabric: "[Cotton Jersey from Minerva](https://www.minerva.com/mp/1133057/100-cotton-jersey-stretch-knit-fabric-navy-blue&variant=1096135)"
pattern: https://merchantandmills.com/store/patterns/pdf/the-tee-shirt-2/
colour: Navy blue with navy ribbing
tags:
  - t-shirt
  - indie pattern
draft: false
---

Another t-shirt using the Merchant & Mills pattern. Some of the problems I had with the first attempt were

## Design

As with my first attempt, I included the front pocket and cut it in the 38" size.

## Construction

I followed the same process as the first attempt, sewing and then overlocking the seams. I considered going straight for the overlocker but wasn't quite confident enough.

I feel like I did a bit better with the neck band this time, but again there were signs of puckering that suggested I could have distributed the ease a bit more evenly.

## Summary

Another very wearable t-shirt and some good practice.
