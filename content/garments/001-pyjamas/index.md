---
title: "Pyjamas"
date: 2021-02-28T22:09:11Z
number: "001"
made: 2021-02-27
fabric: "[Space cotton wine](https://www.simplyfabrics.co.uk/product/ex-designer-space-cotton-wine/)"
pattern: "[Simplicity 8179](https://www.simplicity.com/simplicity-storefront-catalog/patterns/women/cozywear/simplicity-pattern--8179-child-teen-and-adult-lounge-pants/)"
colour: "Purple/mauve"
tags:
  - pyjamas
  - trousers
  - cotton
  - commercial pattern
draft: false
---

My first constructed garment was a pair of pyjama bottoms, made using Simplicity 8179. I made them in a size medium, using a purple printed cotton from [Simply Fabrics](https://www.simplyfabrics.co.uk).

## Design

This was a fairly simple pattern that included pockets, an elasticated waist and leg cuffs.

## Construction

### Assembly

Since this was my first ever attempt, I don't think there is a lot of value to be added by talking about how easy or difficult this was to assemble. I achieved it in a long day, and the end result looks like trousers, so that's about all there is to say.

### Critique

Looking at my efforts a bit more closely, there are a couple of points I can learn from:

- Seams got stitched into waist and leg cuffs on the wrong side
- Pockets got caught in waist band
- Misaligned edge stitching on leg cuffs
- Loose slip stitching

{{<figure src="001-seam-allowance.jpg" title="Notice how the seam doesn't lie flat because it is caught in the waistband" >}}

{{<figure src="001-pocket.jpg" title="The same happened with the pockets – the fabric seems to get caught when it hits the stitch plate" >}}

{{<figure src="001-hem.jpg" title="Top stitch needs some practice – it doesn't quite line up!" >}}

{{<figure src="001-slip-stitch.jpg" title="Closing this opening could have been neater and tighter" >}}

## Summary

It's fair to say that I was very pleased with this outcome. I didn't expect to produce a perfect garment with my first attempt, but I didn't expect it to turn out this well either. The fact that I even have a wearable item that I made myself is very exciting.
