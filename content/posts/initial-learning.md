---
title: "Learning about sewing"
date: 2021-02-21T18:38:39Z
updated: 2021-03-01
draft: false
---

When I first looked at the idea of sewing, I didn't have many ideas beyond making a pair of pyjamas and maybe some sweatpants. Perhaps I could also alter some clothes; maybe one day I could make a shirt. But where to start?

## Market research

There were two major areas to research:

1. Machinery and equipment
2. Patterns and designs

I'm very comfortable looking at machines, their reviews, obsessing over details and lusting over equipment that I simply do not need. So the research into getting a sewing machine was never going to be a challenge.

Looking for something to make was a bit of a different story. On the one hand, the number of fabric suppliers and haberdasheries is impressive. On the other hand, most of the patterns and inspiration I found at first was aimed at women.

> I realised I was not the target market for these magazines

As well as looking online, I sampled the magazine market here in the UK. Of the three magazines I bought, each came with a couple of free patterns, plenty of free online resources, and was a fairly feature-packed magazine. However, it was clear that I was not part of the target audience. The magazines were not aimed at men; also curious, but clearly not a problem for me at the moment, there seemed to be a strong focus on beginners and much less, if anything, about more advanced sewing.

{{<figure src="/images/magazine_patterns.jpeg" title="I don't think I am the target market for these magazines">}}

### Now I pause to acknowledge the irony

I realise that there is a lot of irony about highlighting the lack of men's content anywhere on the internet. We are certainly not under-represented. Nevertheless, my early research online and looking at the sewing magazine market made me think that home sewing for men was fairly non-existent.

## Digging deeper

OK, I did manage to find some patterns. But without the easy access to resources that I was expecting, I ended up looking more widely, and I probably came out better as a result.

Firstly, although many of the websites and YouTube channels I found were written by women and dealt with women's clothes, for the more technique-focused blogs and videos, this didn't matter. With so much for me to learn it was almost irrelevant that the garment being produced was a dress rather than trousers.

### Pattern making

Before too long, I discovered that pattern-making was a thing. In fact, for some people, the idea of buying commercial patterns seemed to miss the point of making your own clothes entirely. This sparked an interest in me, especially when I started to find some resources that explained how patterns are constructed and why.

Now, in addition to learning how to make clothes, I was adding the challenge of learning how to design them. To do that, I needed to learn how to measure people, and with the country enduring its third lockdown, that means that I had to learn how to take many measurements of myself. Quite a jump in complexity from the lofty thoughts of sewing a few seams at the start of the year!

### There are some male-focused blogs!

Finally, having virtually resigned myself to the idea that there were no other men sewing, I started to find a few sewing blogs written about mens' clothing. Not many, but enough to give me a bit more hope that this wasn't a journey I'd have to make alone.

## My plans

This is not my first experience of jumping head-first into a new hobby. In the same way as my other experiences, I like to explore several different areas at once. That way, if I get bored or frustrated with what I'm doing, I can jump across to something else that's still part of the hobby.

### Learn how to make patterns from scratch

I like to learn by trying to understand the fundamentals of the subject. Something about doing something without understanding why just doesn't sit well with me. Therefore, I will try to understand how measurements become patterns, and how patterns become garments.

A great example of this way of learning comes from a description I found about darts. Until recently, I saw darts in some garments but not in others. I vaguely knew what they did but not _really_. Then I read about taking a piece of paper and bending it. It will bend one way or the other, but not both at the same time without creasing. However, if you cut a thin triangle out of one edge, close the gap and tape it, your paper now curves in two directions; you have created a dart. This small piece of information was perfect for me -- if I can learn more about why patterns are the way they are, I should feel much more confident about making and altering clothes.

### Look into children's clothing

I have a young niece and nephew. Could I make clothes for them? Possibly. And they're small, so the fabric costs should be lower!

### Start sewing

Finally of course, I need to actually start sewing and make something. I have a pattern for some pyjamas, I've got the fabric and the thread, so just need to do it.

## Resources

To end this long post, I have listed the resources I've found so far that I enjoy.

### Books

- Aldrich, Winifred. *Metric Pattern Cutting for Menswear*. United Kingdom: Wiley, 2011.
- McCunn, Don. *How to Make Sewing Patterns, Second Edition*. United States: Design Enterprises of San Francisco, 2016.

### Blogs

- [Male Pattern Boldness](http://malepatternboldness.blogspot.com/), http://malepatternboldness.blogspot.com/
- [The Creative Curator](https://www.thecreativecurator.com/), https://www.thecreativecurator.com/
- [Line of Selvage](http://lineofselvage.blog/), http://lineofselvage.blog/
- [Male Devon Sewing](https://maledevonsewing.co.uk/), https://maledevonsewing.co.uk/
- [Mainely Menswear](https://mainelymenswear.com/), https://mainelymenswear.com/
