---
title: "Spring 2022 Update"
date: 2022-04-02T19:30:10Z
tags: 
 - diary
draft: false
---
Here is a rough summary of the past six months of sewing.

## Parachutes

My friend asked me if I could make a large parachute for a children's activity group. I made two -- one was a 2-metre diameter test that I gave to my niece and nephew, and then I made the full-size version. 

The concept for the parachute was simple and it consisted of 6 or 8 segments sewn together with the seams and edges all bound. The tricky parts of the work were mostly due to the sizes involved, with long seams and a lot of fabric to manage. With my industrial machine, I had the option to buy a binding attachment. For the most part this worked very well, but I did need to pay more careful attention than I expected in order to avoid the main fabric being missed by the binding. However, the most awkward part of the whole job was cutting out such a large amount of fabric -- I just don't have the space to lay out such huge pieces!

## Mini Hudsons

With some leftover fleece from my own Hudsons, I made a tiny pair for my nephew, and he loves them. One of the things he likes most is the pockets. A lot of his clothes have patch pockets but the front slash pockets are a real novelty for him.

After the success of the first pair, I decided to make some more for him and looked for some colourful prints for him. Apart from the small size of some of the pieces, this pattern is really easy to make, and I put it all together on my overlocker. The one exception to that was the waistband. Fleece fabric is quite bulky and after hearing about a different technique, I decided to try something different. Rather than assemble the waistband casing as one piece and then attach that to the trousers, I only attached one side of the waistband directly, applying binding to the other seam and then sewing that down. This reduces bulk but it was quite tricky to do on a stretchy fabric.  

## Sweatshirt

I had some lovely dusty pink sweatshirt fabric for a jumper that I finally got around to making. Unfortunately it's far too short for me! I made the pattern without measuring anything, so I only have myself to blame, but I am so used to being a very average size that it's not been a problem so far. It's such a shame but I might try to do something to make the sweatshirt more practical -- colour blocking perhaps?

## Apron

I made the Dogwood Apron from Helen's Closet, using a lovely twill fabric from Merchant and Mills. It turned out very nicely for me, and was a nice thing to make to practise my existing skills as well as learn some new ones. 

## Bag

Knitting is my new hobby for 2022, and although I bought the pattern a while ago, I finally had a good reason to make the Field Bag from Grainline Studio. I managed to make it in a day, and I'm pleased with the result. It cost me two needles though, as the fabric layers really added up to a difficult thickness to manage. 

## Shorts

There have been vague hints that warmer weather is on the horizon, and I love wearing shorts when I can. I am on the lookout for a nice pair of tailored shorts to buy and/or make. I haven't found much yet, but hopefully I will do soon.
## Patterns

Thinking about patterns, and men's patterns in particular, I realise that I often find new potential sources for indie patterns before forgetting them. I would like to see if it's possible to put together a database of patterns that are appropriate for me...

