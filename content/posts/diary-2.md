---
title: "Further Progress"
date: 2021-03-20T21:28:21Z
tags:
  - Diary
draft: false
---

## What I did this week

### Fabric Shopping

I looked through the vast amounts of printed cotton poplin for sale in order to choose something nice and summery for my niece's dress. With so many to choose from, I decided to create a shortlist on a mood board, trying out the free [Moodboard](http://www.gomoodboard.com/) site. It works quite nicely when you also have the photos of the dress being modelled alongside.

In the end I narrowed down a selection of about six patterns, and ordered some samples.

### Overlocker Practice

There wasn't much structure to this -- I just ran fabric through the machine to see what happened! Seriously though, I was impressed by both the high speed of the machine but also the good control at lower speeds.

I did find myself breaking the lower looper thread on a couple of occasions. I think this happened when I tried to pull the finished fabric out from the machine, much like I do when I finish sewing. Because there is a lot going on, it's not as simple as making sure the stitch is completed and the tension released. I think I just need to get used to running the machine until the fabric is free from the presser foot.

Once I had practised on scrap fabric, I finished some of the seams in my pyjama bottoms. Clearly this was too late in the construction process to finish the seams properly, but I was still able to get most of the long leg seams finished quite neatly, and they look good.

### Trouser Block Progress

I transferred my 2.5-cm darts to my pattern, and started to tackle the side seams. Following Don McCunn's book is generally very clear, but I have struggled to follow this step. Part of it is because the pants sloper McCunn describes is based on the previous skirt sloper. So to finish the side seam for the trousers, the reader is referred to the skirt process. Unfortunately there is quite a difference in the two, such as the skirt sloper not having had its side seams sewn, and also being made from gingham. This means that the aim for the skirt sloper is to ensure the grain is square. The trousers are not able to do this because of their shape.

Despite the confusion, I have got a fit that I am quite pleased with, and so the next step is to deal with the ease...

## What I'd like to do next

- Finish trouser block and sew for second fitting
- Choose fabric for dress
- Buy a t-shirt pattern (probably [this one](https://merchantandmills.com/store/patterns/pdf/the-tee-shirt-2/))
