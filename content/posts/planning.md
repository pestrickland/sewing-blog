---
title: "Planning"
date: 2021-03-10T22:24:47Z
tags:
  - Research
draft: false
---

A couple of weeks after sewing my pyjama bottoms, I've been wondering what I need to do next.

## More Research

I read _a lot_, and the internet is a big place. As I've said before, there are a lot of resources about this hobby, but most of it is aimed at women's clothing. There is a huge amount of excellent content for women's garment-making. Not only are there the commercial patterns (the "big 4" of Butterick, McCall's, Simplicity, and Vogue), but so many exciting indie pattern makers. A bit disappointingly but unsurprising, there are only a few menswear patterns amongst the indie makers.

In addition to the various blogs I found earlier this year, I've been sampling a few podcasts and YouTube channels. This has helped to increase my awareness of the different things that people do with their sewing hobby, and that's been quite helpful, allowing me to consider a few options.

## Learn and Practise Techniques

Although I do like to read and understand the theory of how things work, ultimately sewing is a practical hobby, and I need to sew. One thing that I could do, is practise technique with no final garment in mind. But to be honest that does not inspire me whatsoever. In a way, it reminds me of practising handwriting at school, endlessly writing sequences of the same letter to learn the shape.

### Sewing Knits

I'm sure that pure technique practice is invaluable, but I don't think it's something I want to try at the moment. (However, my handwriting _is_ terrible, so maybe I should reconsider this in future.) One thing that I am interested in, however, is sewing knits. I mention this below, but so many of my everyday clothes are made from knitted fabrics that it would open up more possibilities if I learned how sewing knitted fabrics differed from sewing woven cloth.

### Drafting Pattern Blocks

It's not just sewing of course. I want to be able to draft my own patterns. Being a man seems to be beneficial here for a couple of reasons. Firstly, my body shape is not as complex, and secondly, there isn't a huge variety of clothing styles. Therefore if I am able to draft some well-fitting pattern blocks, I should have a really strong foundation to make any style of garment that takes my interest.

Once I get to grips with pattern blocks (I'm reading Winifred Aldrich's book at the moment), I would also like to look at constructing children's clothing for my younger relatives.

### Understand Fitting

Fitting ties closely into pattern drafting I think. As I read more about the subject, I'm starting to understand how fitting works, such as what seams do and how they can be altered. But it's only going to be by actually cutting out patterns, sewing them, fitting them and adjusting them that I really understand what's happening.

## Make a Wardrobe?

One theme that seems to be quite common amongst hobbyists is the idea of making your own wardrobe. It seems like a very good idea. Not only is it a sizeable challenge, but you could end up with a range of garments, and learn a lot along the way. My wardrobe doesn't have a lot of variety, but there is still quite a lot to consider:

- T-shirts
- Underwear
- Sweatshirts
- Shirts
- Shorts
- Chino trousers
- Sweatpants
- Jeans
- Wool trousers
- Suits

There are a lot of options, but I'll look at three to start with.

### T-shirts

T-shirts seem to be a good candidate the more I think about it. I wear them all the time, and apart from some screen printed examples I am always keen on plain colours. It would be a challenge to start to learn about knits, sleeves and so on, and once I get to a point with a pattern that I am happy with, I should have the means to make as many new t-shirts as I need. This seems to be a great idea.

### Shorts

In the not-to-distant future the weather is likely to get warmer as summer approaches. There should be enough time to work out all the details and skills needed to make a pair of tailored shorts shouldn't there?

### Shirts

If I ever spend considerable time back in the office, I'd like some new shirts to wear. I've always had an appreciation for a nice shirt, so at some point I am going to tackle this. There does seem to be a lot going on with a shirt though, so I think this one will need to be a later project.

## Summary

I haven't really produced a _plan_ yet, more the start of one. I have a few things that I can look into a bit more and hopefully arrange into some sort of plan:

- Develop pattern blocks and fit them to my body as well as I can
- Learn about sewing knits and make a t-shirt
- Look into what would be required to make a pair of tailored shorts in time for summer

There's enough of a challenge there, hopefully enough time to do it all, and hopefully enough variety to keep my interest high.
