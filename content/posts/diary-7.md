---
title: "Machine covers complete"
date: 2021-04-25T15:29:23+01:00
tags:
    - Diary
    - Making
draft: false
---

## What I Did This Week

### Made a Sewing Machine Cover

Similar to last week, I followed the Closet Core Patterns guide to making a cover for my sewing machine. I used the same type of fabric as for the overlocker cover, only in a different colour. 

I was surprised how long it took me to make the cover last week, so rather than try to do it all in one go, I did the tasks piecemeal throughout the week. This allowed me to get some sewing done in the evenings, and by Saturday I just had the hem to finish. 

Since I knew what I was doing with the pattern for the second cover, I was a bit disappointed to find that one of the end pieces was noticeably shorter than the other. I don't know when this happened, but since I had gone to quite a bit of trouble to align the pattern it was quite a surprise. I also found that there wasn't much excess material to make a neat hem, so it's a poorer finish on the inside than the overlocker cover. All that said, I am still pleased, and I now have two nice covers for my machines.

{{<figure src="/images/machine-covers.jpg" title="The two completed covers" >}}

## What I'd Like to Do Next

- I still need to make the two t-shirts I bought fabric for last week
- Read more of my pattern cutting books