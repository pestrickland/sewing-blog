---
title: "The Second T-Shirt"
date: 2021-05-03T14:33:23+01:00
tags:
  - Diary
  - Making
draft: false
---

## What I Did This Week

### Made a Second T-Shirt

Taking advantage of a longer weekend, I decided to make a second t-shirt in one day. As I've mentioned previously, I wanted to repeat a pattern to get an idea of how consistent I can be, and how long it can take to make something I am more familiar with. It was an interesting experience.

I made a navy blue t-shirt and there were a couple of changes to the previous attempt. First, I remembered to prewash the fabric; second, the fabric was less "premium" than the my grey t-shirt.

Pre-washing: that essential step that I missed on my grey t-shirt (fortunately I haven't noticed any problems as a result). This was my first experience at washing a knitted fabric, and the amount that it curled up really surprised me. It's great that knitted cotton doesn't fray, but the curling became very annoying. From cutting and pinning, to sewing and overlocking, the curl got in the way everywhere. The most annoying part was that I couldn't sew the final hem without the raw edge curling so much that it caught under the needle. In the end I needed to unpick it and sew it again.

Before finishing the hem a second time, I did a bit of research and found that starch can be helpful. Therefore I made some spray starch, applied it and had a much more workable piece of fabric. That's something to remember for the next t-shirt and any washed knit.

The fabric itself feels a bit thinner than the "premium" jersey I used before, but it is still soft and comfortable to wear. It'll be interesting to see how it feels after washing.

As for the comparison between the two t-shirts, I'm pleasantly surprised. The shoulder seams were almost identical, and the sleeve lengths were also close between the two. However, there was quite a difference in the width of the sleeve. I have no idea how that happened, but it's only noticeable by direct comparison.

Overall I'm pleased. I managed to produce a similar-fitting t-shirt in a day, learned a bit more about the way the fabric handles and ended up with something else to wear!

## What I'd Like to Do Next

I think it's time for a bit more theory and preparation for making other clothes. Therefore, I want to:

- Continue following the lessons in my sewing book
- Start drafting basic blocks for children's clothes (in the 1--4 year range)
- I've also been helping my mum with some embroidery digitisation, so would like to learn more about that
