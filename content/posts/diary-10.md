---
title: "Slow and Steady"
date: 2021-05-18T21:14:10Z
tags:
  - Diary
  - Drafting
draft: false
---

## What I Did This Week

### More Block Drafting

I spent more time organising basic blocks for children. Mostly, this consisted of changing the organisation of the blocks into a single file. One of the drawbacks of Seamly2D is that you cannot easily copy a block from one file to another. So, whereas I thought I would be able to start a new pattern and import a body block from one file and a trouser block from another, it will actually be easier to make a copy of a file of *all* the blocks and work with the ones I need. It was a little bit annoying, but it's just part of refining my workflow.

### Refining PDF Generation Workflow

I can export an SVG file from Seamly2D, manipulated it as requited in Inkscape and then output a PDF. Previously I would then slice up my PDF into A4 sections using [PosteRazor](https://posterazor.sourceforge.io/). This worked OK, but matching up the trimmed A4 sheets accurately was time-consuming. 

I found a [video](https://www.youtube.com/watch?v=i2OFr1pVxuA) on YouTube that shows a more robust method of producing tiled patterns, albeit much more manual. You essentially export cropped sections of the large pattern to individual PDF files. This does allow you to reliably add things such as crop marks and alignment marks, and ultimately you get a better looking result. 

## What I'd Like to Do Next

* Continue following the lessons in my sewing book
* Start designing a boiler suit/flight suit/overalls for my nephew