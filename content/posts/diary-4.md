---
title: "Easter sewing"
date: 2021-04-05T19:44:59+01:00
tags:
    - Diary
draft: false
---

## What I did this week

### Made a dress

Spending Easter with my family, I helped my mum make up the dress we'd chosen for my niece. We used a light cotton poplin and the result was a very pleasing fluttery summer dress with simple lines and a couple of patch pockets. It was also my first experience of using the overlocker to finish seams in the right order during construction. It worked so well, and my mum is now on the lookout for an overlocker of her own.

### Made a T-shirt

I eventually made the t-short I ordered. I'm really pleased with the result but I didn't manage to get it done when planned. 

### Stuffed the Tailor's Hams

The hams I sewed last week are huge and took a lot of stuffing. I used wood shavings that we usually use for animal bedding. Because they can be compressed so much, it felt like I was filling each one about three times over! 