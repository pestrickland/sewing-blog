---
title: "Slow progress, but still progress"
date: 2021-03-18T11:23:41Z
updated: 2021-03-22
tags:
  - Diary
draft: false
---

With so much to learn and do, it feels a lot like I am struggling just to keep still. But I think if I keep a diary of what I have been doing, I will see that it is all progress, even if it's slower than I'd like.

## What I did this week

### Cutting and Basting a New Pattern Block

Working through Don McCunn's book, I finally got around to cutting out a trouser block pattern and basting it together.

### Buying an Overlocker

I was looking at overlockers for a couple of months, and after it seemed like there was a global shortage I started looking more widely at was was available. I ended up finding a good price on a Jaguar 489 and bought it quite quickly. The first one I received was damaged but the wonderful people at [Gur Sewing Machines](https://www.gursewingmachines.com/) had a replacement with me the very next day, and collected the damaged one too. I was very impressed.

I practised using the overlocker on the raw edges of my muslin. It's very fast and worked quite well, although I'm aware there is a lot to learn about what I can do with it.

### Starting to Fit the Trouser Block

Fitting so far has consisted of locating dart points and trying to work out how wide they need to be. Unfortunately the book only describes hip darts for a skirt using a gingham pattern, since everything is square. Trousers have the centre back seam cut at a bias, and so it seems that judging the two hip darts will just have to be done by eye for now. I ended up going with 2.5 cm/1 in darts.

### Looking for Fabric for a Summer Dress

My mum bought a pattern for my niece at the end of last year. I'll be visiting my mum over Easter so we thought we should get the dress made. I volunteered to look for some nice printed cotton. So far I've found some nice poplin but have a few more fabric shops to check out.

## What I'd like to do next

Off the top of my head, these are the things I want to do next:

- Fit Side Seams of Trouser Sloper
- Practise with the Overlocker
- Cut Pattern for Tailor's Ham
- Order Fabric Samples for Dress
- Choose t-shirt pattern to make
- Choose sweatpants pattern to make – probably the [Hudson pant](https://shop.truebias.com/product/men-s-hudson-pant) from [True Bias](https://shop.truebias.com/)

## Summary

This is just a quick diary entry, partly to record what I was doing when, but also to try to encourage me to focus on the things I've planned, rather than dashing off to the next exciting thing. I'll try to keep up this idea every week or so.
