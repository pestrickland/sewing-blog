---
title: "Drawing Flats"
date: 2021-05-25T19:04:00Z
tags:
  - Diary
  - Drafting
draft: false
---

## What I Did This Week

### Drawing Flats

I don't seem to have achieved much this week. In fact the only thing I can think of was producing some flat templates and attempting to visualise the idea I have for some overalls for my nephew. I bought some children's templates from Etsy but spent some time tracing them into Inkscape to give some additional flexibility, such as picking and choosing between arm and leg poses.

Once I had my templates, I found the drawing process quite enjoyable. Compared to sketching, the more technical approach with the flat look made me think more about how the clothes would be constructed, and the tools gave me a quick way to make something that looks good.

So far, I have a design that has a few pockets and an integral velcro waistband.

{{<figure src="overalls-flat.png" title="Work in progress" >}}

## What I'd Like to Do Next

- Draft a pattern for the overalls
