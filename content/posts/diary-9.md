---
title: "Pattern Drafting"
date: 2021-05-10T23:07:23+01:00
tags:
  - Diary
  - Drafting
draft: false
---

## What I Did This Week

### Drafting Basic Blocks for Children's Clothes

I spent a considerable amount of time drafting the basic blocks for children's clothes. I followed the instructions in Winifred Aldrich's *Metric Pattern Cutting for Children's Wear and Babywear* book, and used Seamly2D for drafting. The combination of those two means that I now have some parametric (or multisized) pattern blocks to develop into garments. The sizes used by Aldrich are for boys and girls from approximately 3--14 (although the sizes are actually based on heights rather than ages). Seamly2D uses the measurements as variables rather than just having to pick specific numbers. This way, I can select a height and the pattern will grow or shrink according to the dimensions required.

The basic blocks I've drafted so far are:

* Body block and sleeve
* Overgarment
* Easy-fitting trousers

The pattern drafting process in Seamly2D is similar to drawing by hand, with just a few quirks. Two of the more difficult ones are pivoting a line to touch another line, and curves.

On paper, you could pivot a line by measuring the length of the line and rotating your ruler around the pivot point until it touches the line it needs to connect to. The process in Seamly2D is to use a circle or arc for this: the centre of the arc is the pivot point, the radius is the length of the original line, and you identify a point where the circle or arc intersects with the line that needs to be connected to. Finally, a straight line can be drawn between these two points. [Minimalistmachinist](https://instagram.com/minimalistmachinist) has a [series on YouTube](https://youtube.com/playlist?list=PLRSS4yRTGLwLh-i3JI_Zy-Vzr9f-uQBLT) that goes through drafting a bodice block that is well worth watching if you're interested in learning more about the whole process.

Curves would be sketched or drawn with pattern curves by hand. Splines are used for this in Seamly2D and whilst that is fine to do by eye, there an additional amount of work to do if you'd like the spline to scale properly as you change the size of the pattern. Essentially it involves a lot of construction lines to parametrise the control points that define the spline. There is more information about this on the wiki [here](https://wiki.seamly.net/wiki/Creating_Smooth_Curves:_The_Kolson_Method).

## What I'd Like to Do Next

I think it's time for a bit more theory and preparation for making other clothes. Therefore, I want to:

- Continue following the lessons in my sewing book
- Start designing overalls or a flight suit for my nephew
- Improve my pattern workflow, especially printing tiled PDFs
