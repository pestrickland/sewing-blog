---
title: "Thoughts on a wardrobe"
date: 2021-04-14T20:22:47+01:00
tags:
  - Planning
  - Wardrobe
draft: false
---

After enjoying the experience of my t-shirt, and deciding that I needed to reproduce the effort, I started to think more about what I could make in terms of daily clothing. What would I need to make for a complete wardrobe of essentials? Here is my thinking to date...

## Basic garments

My wardrobe isn't that sophisticated these days, so starting off with t-shirts is a good bet. They can be worn as a base under jumpers and sweatshirts, or just on their own. A well-fitting plain t-shirt is often good enough on its own, but a lot of my current ones are screen printed or have a couple of details such as embroidery. I don't think I want to consider such embellishments at the moment, but perhaps some colour blocking would be possible for a little bit of variation ― the two-tone raglan-sleeve t-shirt would a nice project. For a wardrobe, say one for each day of the week:

- 5 plain t-shirts
- 2 multi-coloured/embellished t-shirts

Underwear is another staple of course. Socks I have no interest in considering making, but I would like to try some boxer-briefs. There is a surprising variety of cuts and fits for these, so trying to replicate my favourites would be an interesting challenge.

## Trousers

Jeans, chinos, shorts and sweatpants.

- 2 pairs of jeans ― one heavier raw denim and one lighter denim maybe with some stretch
- 3 pairs of chinos for work ― interesting colours with bespoke detailing such as contrast buttonholes, flowery pocket linings, Hong Kong seams and cuffable hems
- 2 pairs of sweatpants ― comfy, stretchy but not too thick
- 3 pairs of shorts ― similar cut to the chinos no longer than the knee. Bright colours, stripes and printed fabric to be considered

## Shirts

- 5 shirts for work ― formal tailored fit. At least one double cuff. Lots of details. Mostly poplin or Oxford cottons
- 1 or 2 more casual shirts. Linen?

## Sportswear

- Base layers ―short and long-sleeved merino ideally
- Running shorts
- Running t-shirt

## Outerwear

- A chore jacket
- Short blouson
