---
author: Paul
title: Beginnings
date: 2021-02-13
---

Finally the fabric for my first project has arrived, I've ordered some thread and I have a means to cut the pattern accurately. Now to begin...

I am jumping ahead of myself a bit here. Let's look back a few months.

{{<figure src="/images/first_sloper.jpeg" title="My first sloper/trouser block" alt="My first sloper/trouser block">}}

## Sewing machines are magic

A few weeks ago I bought my first sewing machine. Much of my spare time in 2021 was spent looking at machines, reviews, prices and all the new terminology around sewing, dressmaking and tailoring.

When I was a child I remember using my mum's sewing machine. Not for anything constructive but because it was a machine with complicated moving parts and somehow it made stitches. More recently my Mum bought an embroidery machine, and I spent quite a few hours with her so that we could learn how to use it.

I didn't realise it back in November, but the sewing bug had taken hold. When I returned to my parents' house over Christmas, I made my first face covering, and the magic that is lockstitch continued to amaze me.

## Clothing is incredible if you take the time to look

Although I haven't made clothes before, I have had an interest in clothing since my student holidays were spent working in a menswear shop. The daily comings and goings of customers looking for jackets and trousers, shirts and ties, and shoes and socks left me with a keen interest in how people look in their outfits.

Leaving university and starting my career engineering dampened that interest, and real life took over. I kept my appreciation for well-made clothes and shoes, but certainly didn't spend a lot of time on the subject.

## Companies with a different focus

A couple of years ago, I listened to a mini-series on the excellent [99% Invisible](https://99percentinvisible.org/) podcast called [_Articles of Interest_](https://99percentinvisible.org/aoi/). It focused on the clothes we wear: everything from children's clothes to punk, and from jeans to wedding dresses. In turn, that led me to discover jeans made from raw denim and the story of companies such as [Hiut Denim](https://hiutdenim.co.uk/), [Community Clothing](https://communityclothing.co.uk/) and [Paynter Jacket Co.](https://paynterjacket.com/). Each company puts more emphasis on the clothes, the process and the people behind them than we've become used to.

All of this interest must gradually have taken hold of me, and since the start of 2021 I sought out as many resources as possible, such as fabric suppliers, pattern-making tools and books, blogs and YouTube channels.

## What I want to achieve

In typical fashion, so far I have only been a theoretical sewer/dressmaker/pattern-maker/tailor. It has taken a while to find and gather the necessary tools and materials, but I need to break out into actually constructing something. There are two routes I want to take:

1. Make some pyjamas from a [commercial pattern](https://www.simplicity.com/simplicity-storefront-catalog/patterns/women/cozywear/simplicity-pattern--8179-child-teen-and-adult-lounge-pants/)
2. Make a pattern block based on my measurements and construct a toile to build on

And so that brings us up to date. I have a pattern, fabric, thread, scissors, machine, pins, and an iron. Now I just need to take the plunge...
