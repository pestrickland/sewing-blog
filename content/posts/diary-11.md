---
title: "So much to learn"
date: 2021-06-06T20:52:10Z
tags:
  - Diary
  - Drafting
draft: false
---

## What I Did This Week

### Taking the Plunge

After spending a lot of time procrastinating with the design of my flight suit pattern, I finally took the plunge and printed it out and cut some cloth. It isn't complete yet, but I was curious to see if and how the shapes I had drawn could fit together.

Because this is a children's pattern, the pieces are tiny, and taping together the pages didn't take long, nor did cutting it out. Before long I had a small pile of pieces and no clue how to assemble them.

I love this project so far and especially what it has taught me: that there is _so much to learn_.

I decided to put the trousers together first. I used a very simple one-piece trouser pattern so simply had two legs to sew together. Even that caused me to think about the order of construction though. I stitching the inseam of each leg was fine, but what to about finishing the seams? For practice, I pressed the sames open to give me more to overlock, but I couldn't work out when to finish each seam. I sewed the two legs together but then realised that, whichever order I finished the seams (either the inseams or the crotch seam) I wouldn't be able to do it neatly since one seam would be caught by the perpendicular one. What I should have done, I think, is finish the inseams before sewing the two legs together.

With a tiny pair of trousers sewn up, I started work on a basic pocket I had drawn. Not a lot to say there except again, I need to learn more about the order of operations and think a lot more about how to enclose raw edges.

I put the body pieces together quite neatly and attached the trousers. It was reassuring to see how relatively easily I could assemble a garment. The sleeves were tricky and I'm not sure that I did them correctly -- this was my first attempt at sewing woven sleeves compared to jersey.

Finally, I was left wondering what I needed to do for the cuffs and the collar. I eventually worked out how to approximate a cuff, but the collar remains a mystery. All in all it was a tiring but exciting weekend of learning and sewing!

## What I'd Like to Do Next

- Continue following the lessons in my sewing book
- Read about collars
