---
title: "Summer update"
date: 2021-09-02T20:55:10Z
tags:
  - Diary
draft: false
---

## What I've been up to

It's been a while, and there are now a few sewing-related updates to cover.

### Making the Hudson Pants

I made a [test version]({{< ref "003-hudson-pants" >}}) of the Hudson sweatpants using some petrol fleece. I say test version, because I have another fabric choice in mind but it is quite expensive so wanted to practise first.

Once I'd worked out how to assemble the trim on the pockets, I found the assembly quite straightforward. I did have to open up the waistband to shorten the elastic, however, and when I tried to sew it back together I didn't quite catch the seam all the way around. It's quite annoying but a nice reminder to be careful next time.

The pattern as I cut it is quite short. I would consider lengthening it by a couple of inches next time I think.

### Making a Pressing Mat

I have been trying to set up more of an established sewing space without intruding too much into my living space (even though the hobby has invaded most of my life...) With that in mind, I bought a big cutting board to fit the whole of my table. Previously I had an A1 size board, which was fine until you have to move it around under the fabric when cutting out. Having a whole table to cut on makes that chore a little bit easier.

The mat I bought came from [Rhino](https://rhinocuttingmat.co.uk/product/rhino-heavy-duty-cutting-mat/), and I went for the more economical heavy duty mat, cut to 120x60 cm. So far I'm very pleased with it.

With a cutting table set up, I wondered if I could also make use of it for pressing. I have a decent ironing board, but it's just another thing I need to have in my room and it is not as big as the table. So I did some research and fairly impulsively bought a collection of fabric and batting on a random Friday afternoon of shopping. (Note to self, make a list before you shop next time...)

Cutting the layers out was some of my best work. Unfortunately I hadn't anticipated the amount of movement of those layers even when basting it together. Once assembled I quilted the mat together and then had a bit of a disaster binding it all! I thought I needed to use wide binding but in hindsight it would have been easier to use the size I am used to so that I didn't have to worry about where I was positioning the presser foot. So I'm not too happy with the result, but I do have a mat to try out for my next project.

### Prototyping the Bruce Boxers

Another avenue I have been exploring is on [Freesewing.org](https://freesewing.org). This is an interesting resource full of free sewing patterns that are generated on the fly according to a size you pick. More usefully, you can add your own measurements to get a size tailored to you.

The Bruce boxers are a trunk-style boxer and I had a go at making them with some spare fabric. The pattern generation was easy to do and the assembly instructions were also quite clear. It is made almost entirely on an overlocker, and despite that being a first for me, it was quite straightforward to do, if a bit fiddly in places.

The first pattern I cut was a bit big and I failed to cut a small enough waistband -- you really need those to be quite tight I find. After adjusting some of the negative ease I ended up with quite a comfortable pair of boxers that don't look too dissimilar from what I might otherwise wear.

One additional part of this process was in the use of bulky threads for the loopers of the overlocker. This thread is remarkable. It looks unwieldy and fragile, but leaves an incredibly soft finish.

### Buying an Industrial Machine

My first machine was a mechanical Singer machine. It works well and was great to get started with. When I compared it to my mum's Bernina Bernette, however, there were a few things I felt like I would like in my next machine:

- Better speed control, especially at lower speeds
- Control of whether the needle stops up or down
- Automatic knotting/backtack and thread trimmer

I always felt that my machine was almost on or off, with little control in between. Compared to my mum's machine, this was annoying, especially as a beginner, when I was more comfortable with slow sewing.

The position of the needle was less important, but being able to stop at corners with the needle down, or to be able to raise the presser foot to release some tension in the fabric, would be a nice feature.

The final point again was a nice to have, would speed up the process and be very convenient.

I started looking at new machines out of interest, both domestic and industrial, to see what was on offer. What surprised me was that there wasn't really a basic servo-motor domestic machine that didn't also include an awful lot of computerised elements and be heading in the direction of an embroidery machine. The single exception to this that I found was the [Janome HD-9](https://www.janome.co.uk/model-hd9). This was a semi-industrial straight stitch machine that met all my requirements.

Whilst the HD9 met my requirements, I was also interested in a fully-fledged industrial machine, in many ways because of the solid build and my love of engineering. They also sound brilliant. The final surprise in this research was that I could get such a machine at a lower cost than the Janome! I still struggle to work out why.

After all this, I decided that an industrial was what I really wanted, and with some encouragement from my mum to _just buy one_, I ended up treating myself to a [Juki DDL-7000A](https://www.ae-sewingmachines.co.uk/juki-ddl7000a), assembled, mounted on a table and delivered by AE Sewing Machines for less than the HD9. It's a wonderful machine.
