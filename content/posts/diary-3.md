---
title: "It takes longer than you think"
date: 2021-03-29T21:52:57+01:00
tags:
  - Diary
draft: false
---

## What I did this week

### Made Tailor's Hams

Using a [pattern](https://maledevonsewing.co.uk/2018/08/tailors-press-ham-tutorial.html) from [Male Devon Sewing](https://maledevonsewing.co.uk/), I made up two large tailor's hams for pressing. These are big meaty hams, and now need to be stuffed with wood shavings.

{{<figure src="/images/hams.jpeg" title="Two hams, unstuffed" >}}

The process was quite easy, and it was nice to be able to print a pattern, cut fabric and sew it all together in a short space of time. It still surprises me how much time everything does take, but I think that's just the way things are. I'll get faster at some things I'm sure.

I made the ham with 100% wool on one side, and a nice sturdy calico on the other. The wool wasn't cheap, but it feels very nice and was lovely to work with.

### Finalised Side Seam of my Trouser Block

After lots of uncertainty about what I needed to do, I finally decided to call my trousers done and to take measurements for updating my pattern. In the end, I relied more on the look and feel of the fit rather than trying to decode what the book said. And with that, I only needed to take the side seams in by about a centimetre at the waist.

As well as just deciding to go for what looks and feels right, I know that this won't be the perfect pattern for me. I can always do it again, and I'd rather move on to more pattern drafting than get stuck here.

### Chose T-shirt Pattern and Fabric

As previously mentioned, I ended up buying the Merchant & Mills Tee Shirt pattern. I also ordered some anthracite jersey and grey rib. I have a few days off over Easter so will hopefully be able to make a t-shirt in that time.

As well as t-shirt fabric, we also decided on the fabric to use for my niece's dress. Another Easter project all being well.

### Bought Another Pattern Book

This time I picked up Denning Chunman Lo's _Pattern Cutting_, which is a very nicely put together book that seems to be aimed at the fashion student. I'm hoping that it will give me another perspective on the theory of patterns and pattern cutting. It's certainly a nice book to read.

## What I'd like to do next

- Make t-shirt
- Make dress
- Finish hams
- Read more
