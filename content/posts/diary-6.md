---
title: "Shopping and making"
date: 2021-04-18T19:56:30+01:00
tags:
    - Shopping
    - Sewing
draft: false
---

## What I did this week

### Shopped for New Fabric

I previously decided to make a second t-shirt for a few different reasons, one of which was to try to reproduce a similar fitting garment made from the same pattern and to practise being consistent. To do this, I needed more fabric. I ended up buying another 1.2 m of the same type of jersey in a different colour ([Pastel Blue Premium Cotton Jersey](https://www.minerva.com/mp/1193825/premium-cotton-jersey-knit-fabric-pastel-blue&variant=1131409) from Minerva), but also a cheaper fabric for comparison ([Navy Blue Cotton Jersey](https://www.minerva.com/mp/1133057/cotton-jersey-knit-fabric-navy-blue&variant=1096135), also from Minerva). I also bought ribbing for both colours. It'll be interesting to see how they compare.

The other project I was considering was to make covers for my sewing machines. For this, I looked for some canvas with an interesting pattern, but was a disappointed with the selection once floral patterns were eliminated. In the end, however, I found a geometric pattern in colours that I liked, so bought some of each ([Lilac](https://www.minerva.com/mp/1135483/cotton-canvas-fabric-lilac&variant=1113650) and [Rust](https://www.minerva.com/mp/1135483/cotton-canvas-fabric-lilac&variant=1113650) from Minerva). I bought a little bit extra as I needed to make piping for the seams and decided to use the same fabric cut as bias tape.

### Made Piping

This was supposed to just be a step of the overlocker cover project, but it took me a long time. For the whole project I followed [Closet Core Patterns](https://www.closercorepatterns.com) guides. To make piping, I started making bias tape with my printed canvas. I had tried this before just by cutting individual bias strips, but Closet Core Patterns has [a guide](https://www.closetcorepatterns.com/tutorial-how-to-make-bias-tape-piping/) that involves sewing a tube that can then be cut in one continuous strip. This was fiddly to work out and I sewed it incorrectly to begin with. However, I think I understand it enough now to be able to make it more easily in future. 

With the bias tape cut, I sewed in some cotton cord. I didn't have a dedicated presser foot for this so I used a zipper foot. It worked reasonably well but took a while. The fabric I was using also frayed such a lot that I started to worry that I would have none left!

### Made an Overlocker Cover (Eventually)

 With a couple of metres of piping sewn and pattern pieces seemingly fraying in front of my eyes, I finally started to assemble the cover following [the guide](https://www.closetcorepatterns.com/free-pattern-alert-make-our-serger-sewing-machine-cover/). Without the piping it is a simple pattern: two side panels and centre panel sewn between them. The piping was less daunting than I expected, basically being sandwiched between the right sides of the two pieces being joined. I basted it to one piece first, then sewed the other piece. The whole lot was then finished on the overlocker.

With the piping made and sewn in, the hem was the next challenge. I took my time pinning and basting the hem to what seemed to be the right height, checking several times that it looked even. I then pressed the hem lines I needed. The difficulty came from having such a lot of bulk at the corner seams. I should have thought about removing some of the piping cord to reduce the bulk, but it was too late by this point. Fortunately my machine just about managed to get over the corners and I was able to finish the hem without it looking too messy.

## What I'd like to do next

* Make a sewing machine cover in the same way as the overlocker cover
* Make another t-shirt