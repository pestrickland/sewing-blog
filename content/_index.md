---
author: Paul
title: Learning to Sew
---

This blog documents my experience in learning to sew, starting from the very beginning.

You can read about how I became interested at the [beginning]({{<ref "beginnings.md">}}), my [initial learning]({{<ref "initial-learning.md">}}) and see what happens along the way.

So far, I've read a lot, bought quite a lot of fabric, and have a commercial pattern to make. I'm also in the process of making slopers/basic blocks for my own measurements, and just becoming bewildered by all the terminology. It's very exciting!

{{<figure src="/images/linen-cuff.jpg" title="My first attempt at a cuff" alt="Striped linen tunic cuff">}}
